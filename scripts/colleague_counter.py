#!/usr/bin/env python

import rospy
import cv2
import numpy as np
from std_msgs.msg import Int32
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from turtle_bale.srv import *

#################################################################################
#------------------------------- ColleagueCounter -------------------------------
#################################################################################

class ColleagueCounter(object):
  """
    The ColleagueCounter counts the number of verticle lines in a parameter color in a subscribed rgb image topic
  """
  def __init__(self, resultPublisher):   
    self._detections = 0	# How many colleagues do I see?
    self._resultPublisher = resultPublisher
    self._debugPublisher = rospy.Publisher("turtle_bale/debug_image", Image, queue_size=10)
    self._cvBridge = CvBridge()
    self.resetParameters(Empty())
          
#----------------------------------------------------------------------------------

  def resetParameters(self,data):
    if rospy.has_param('turtle_bale/hue_low'):
      self._hueLow = rospy.get_param('turtle_bale/hue_low')
    else:
      self._hueLow = 150
      rospy.set_param('turtle_bale/hue_low', self._hueLow)
    if rospy.has_param('turtle_bale/hue_high'):
      self._hueHigh = rospy.get_param('turtle_bale/hue_high')
    else:
      self._hueHigh = 200
      rospy.set_param('turtle_bale/hue_high', self._hueHigh)
    if rospy.has_param('turtle_bale/saturation_low'):
      self._satLow = rospy.get_param('turtle_bale/saturation_low')
    else:
      self._satLow = 130
      rospy.set_param('turtle_bale/saturation_low', self._satLow)
    if rospy.has_param('turtle_bale/saturation_high'):
      self._satHigh = rospy.get_param('turtle_bale/saturation_high')
    else:
      self._satHigh = 255
      rospy.set_param('turtle_bale/saturation_high', self._satHigh)
    if rospy.has_param('turtle_bale/value_low'):
      self._valLow = rospy.get_param('turtle_bale/value_low')
    else:
      self._valLow = 130
      rospy.set_param('turtle_bale/value_low', self._valLow)
    if rospy.has_param('turtle_bale/value_high'):
      self._valHigh = rospy.get_param('turtle_bale/value_high')
    else:
      self._valHigh = 255
      rospy.set_param('turtle_bale/value_high', self._valHigh)
    if rospy.has_param('turtle_bale/debug_image'):
      self._debug = rospy.get_param('turtle_bale/debug_image')
    else:
      self._debug = False
      rospy.set_param('turtle_bale/debug_image', self._debug)
    if rospy.has_param('turtle_bale/window_rows'):
      self._windowRows = rospy.get_param('turtle_bale/window_rows')
    else:
      self._windowRows = 100
      rospy.set_param('turtle_bale/window_rows', self._windowRows)
    if rospy.has_param('turtle_bale/window_starts_at_row'):
      self._windowStartsAtRow = rospy.get_param('turtle_bale/window_starts_at_row')
    else:
      self._windowStartsAtRow = 50
      rospy.set_param('turtle_bale/window_starts_at_row', self._windowStartsAtRow)
    if rospy.has_param('turtle_bale/window_columns'):
      self._windowColumns = rospy.get_param('turtle_bale/window_columns')
    else:
      self._windowColumns = 640
      rospy.set_param('turtle_bale/window_columns', self._windowColumns)
    if rospy.has_param('turtle_bale/window_starts_at_column'):
      self._windowStartsAtColumns = rospy.get_param('turtle_bale/window_starts_at_column')
    else:
      self._windowStartsAtColumns = 0
      rospy.set_param('turtle_bale/window_starts_at_column', self._windowStartsAtColumns)
    return []
    
#----------------------------------------------------------------------------------

  def callbackRgbImage(self,data):
    try:
      bgrImage = self._cvBridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError, e:
      rospy.loginfo(e)
      
    # crop first self._windowRows rows (out of 480), starting at self._windowStartsAtRow
    hsvImage = cv2.cvtColor(bgrImage,cv2.cv.CV_BGR2HSV)[self._windowStartsAtRow : self._windowStartsAtRow + self._windowRows, self._windowStartsAtColumns : self._windowStartsAtColumns + self._windowColumns]
    
    # set HSV range
    low = np.array([self._hueLow,self._satLow, self._valLow], dtype = "uint8")
    high = np.array([self._hueHigh,self._satHigh, self._valHigh], dtype = "uint8")
        
    # clean out noises and strengthen whatever survives
    dilation = cv2.dilate(cv2.inRange(hsvImage, low, high),np.ones((10,10),np.uint8),iterations = 1)
    closing = cv2.erode(dilation,np.ones((15,15),np.uint8),iterations = 1) 
    
    # find horizontal edges
    sobelx = cv2.Sobel(closing,-1,1,0,ksize=3)
    
    # the sobel function convolves a 3x3 kernel, resulting with two lit pixels for each edge
    result = round(cv2.countNonZero(sobelx)/(self._windowRows*2.0))
    # publish the topic
    self._resultPublisher.publish(int(result))
    
    if self._debug:
      hsvImage = cv2.bitwise_and(hsvImage, hsvImage, mask = sobelx)
      bgrImage = cv2.cvtColor(hsvImage,cv2.cv.CV_HSV2BGR)
      try:
	self._debugPublisher.publish(self._cvBridge.cv2_to_imgmsg(bgrImage, "bgr8"))
      except CvBridgeError, e:
	rospy.loginfo(e)

#----------------------------------------------------------------------------------

  def cleanup(self):
    cv2.destroyAllWindows()
   
#----------------------------------------------------------------------------------
  
if __name__ == '__main__':
  # ros initialization and subscription
  rospy.init_node('colleague_counter', anonymous=False)
  counter = ColleagueCounter(rospy.Publisher("turtle_bale/colleague_count", Int32, queue_size=10))
  rospy.Subscriber("camera/rgb/image_rect_color", Image, counter.callbackRgbImage)
  # initialize the reset_parameters service
  rospy.Service('turtle_bale/counter/reset_parameters', Empty, counter.resetParameters)
  try:
    rospy.spin()
  except rospy.ROSInterruptException:
    rospy.loginfo(e)
  counter.cleanup()
