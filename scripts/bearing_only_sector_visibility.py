#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32,Bool
from geometry_msgs.msg import Twist
from turtle_bale.srv import *

#################################################################################
#---------------------- BearingOnlySectorVisibilityController -------------------
#################################################################################

class BearingOnlySectorVisibilityController(object):
  """
    The BearingOnlySectorVisibilityController class holds all the data and functions required to run a controller node with the bearing only sector visibility algorithm.
  """
  def __init__(self):   
    self._colleagueCounter = 0	# How many colleagues do I see?
    self._collisionLaser = False #Am I about to collide?
    self._collisionBumper = False #Did I collide?
    self.resetParameters(Empty())
          
#----------------------------------------------------------------------------------

  def resetParameters(self,data):
    if rospy.has_param('turtle_bale/radius_big'):
      self._radiusBig = rospy.get_param('turtle_bale/radius_big')
    else:
      self._radiusBig = 1.5
      rospy.set_param('turtle_bale/radius_big', self._radiusBig)
    if rospy.has_param('turtle_bale/radius_small'):
      self._radiusSmall = rospy.get_param('turtle_bale/radius_small')
    else:
      self._radiusSmall = 0.6
      rospy.set_param('turtle_bale/radius_small', self._radiusSmall)
    # In state of equilibrium, the number of turtles in the field of view are colleague_total * field_of_view / 360
    if rospy.has_param('turtle_bale/field_of_view'):
      self._fieldOfView = rospy.get_param('turtle_bale/field_of_view')
    else:
      self._fieldOfView = 62.0
      rospy.set_param('turtle_bale/field_of_view', self._fieldOfView)
    if rospy.has_param('turtle_bale/colleague_total'):
      self._colleagueTotal = rospy.get_param('turtle_bale/colleague_total')
    else:
      self._colleagueTotal = 4
      rospy.set_param('turtle_bale/colleague_total', self._colleagueTotal)
    if rospy.has_param('turtle_bale/linear_velocity'):
      self._linearVelocity = rospy.get_param('turtle_bale/linear_velocity')
    else:
      self._linearVelocity = 0.15
      rospy.set_param('turtle_bale/linear_velocity', self._linearVelocity)
    return []
    
#----------------------------------------------------------------------------------

  def angularVelocity(self):
    res = (self._linearVelocity * 2.0) / (self._radiusBig + self._radiusSmall)
    colleagueEquilibrium = self._colleagueTotal * self._fieldOfView / 360.0
    if (self._colleagueCounter < colleagueEquilibrium):
      res = self._linearVelocity / self._radiusSmall
    elif (self._colleagueCounter > colleagueEquilibrium):
      res = self._linearVelocity / self._radiusBig
    return res
    
#----------------------------------------------------------------------------------
  
  def sleep(self):
    self._rate.sleep()
    
#----------------------------------------------------------------------------------
  
  def publishTwist(self):
    twist = Twist()
    twist.linear.x = self._linearVelocity
    twist.angular.z = self.angularVelocity()
    if (True == self._collisionBumper):
      twist.linear.x = -self._linearVelocity
      twist.angular.z = -2 * self._linearVelocity / self._radiusSmall
    elif (True == self._collisionLaser):
      twist.linear.x = 0
    self._pub.publish(twist)
    
#----------------------------------------------------------------------------------
  
  def callbackColleagueCount(self,data):
    #rospy.loginfo(rospy.get_caller_id() + " counted %d colleagues", data.data)
    self._colleagueCounter = data.data

#----------------------------------------------------------------------------------

  def callbackCollisionDetectionLaser(self,data):
    self._collisionLaser = data.data

 #----------------------------------------------------------------------------------

  def callbackCollisionDetectionBumper(self,data):
    self._collisionBumper = data.data

#----------------------------------------------------------------------------------

  def setPublisher(self,publisher,rate):
    self._pub = publisher
    self._rate = rate

#----------------------------------------------------------------------------------

  def cleanup(self):
    pass

#----------------------------------------------------------------------------------
  
if __name__ == '__main__':
  controller = BearingOnlySectorVisibilityController()
  # ros initialization and subscription
  rospy.init_node('bearing_only_sector_visibility', anonymous=False)
  # initialize the reset_parameters service
  rospy.Service('turtle_bale/controller/reset_parameters', Empty, controller.resetParameters)
  # set publisher to 10 [Hz]
  controller.setPublisher(rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=10),rospy.Rate(10))
  # Subscribe
  rospy.Subscriber("turtle_bale/colleague_count", Int32, controller.callbackColleagueCount)
  rospy.Subscriber("turtle_bale/collision_detection_laser", Bool, controller.callbackCollisionDetectionLaser)
  rospy.Subscriber("turtle_bale/collision_detection_bumper", Bool, controller.callbackCollisionDetectionBumper)
  try:
    while not rospy.is_shutdown():
      controller.publishTwist()
      controller.sleep()
  except rospy.ROSInterruptException:
    pass
  controller.cleanup()
