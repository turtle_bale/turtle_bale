#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import Bool
from sensor_msgs.msg import LaserScan
from kobuki_msgs.msg import BumperEvent
from turtle_bale.srv import *

#################################################################################
#------------------------------- CollisionDetector -------------------------------
#################################################################################

class CollisionDetector(object):
  """
    The CollisionDetector publishes a boolean answer to the question: "Is there an object too close for comfort?"
  """
  def __init__(self, publisherLaser,publisherBumper):   
    self._publisherLaser = publisherLaser
    self._publisherBumper = publisherBumper
    self._bumperPressed = False
    self.resetParameters(Empty())
          
#----------------------------------------------------------------------------------

  def resetParameters(self,data):
    
    if rospy.has_param('turtle_bale/collision_distance'):
      # Any beam shorter than _distance is considered 'positive'
      self._distance = rospy.get_param('turtle_bale/collision_distance')
    else:
      self._distance = 0.8
      rospy.set_param('turtle_bale/collision_distance', self._distance)
      
    if rospy.has_param('turtle_bale/collision_threshold'):
      # The threshold of how many 'positive' beams are considered a detection.
      self._threshold = rospy.get_param('turtle_bale/collision_threshold')
    else:
      self._threshold = 10
      rospy.set_param('turtle_bale/collision_threshold', self._threshold)
      
    if rospy.has_param('turtle_bale/collision_rate_bumper_pressed'):
      # The rate of update when the bumper is pressed
      self._rateBumperPressed = rospy.Rate(rospy.get_param('turtle_bale/collision_rate_bumper_pressed'))
    else:
      rateBumperPressed = 0.5
      rospy.set_param('turtle_bale/collision_rate_bumper_pressed', rateBumperPressed)
      self._rateBumperPressed = rospy.Rate(rateBumperPressed)
            
    if rospy.has_param('turtle_bale/collision_rate_bumper_released'):
      # The rate of update when the bumper is pressed
      self._rateBumperReleased = rospy.Rate(rospy.get_param('turtle_bale/collision_rate_bumper_released'))
    else:
      rateBumperReleased = 5
      rospy.set_param('turtle_bale/collision_rate_bumper_released', rateBumperReleased)
      self._rateBumperReleased = rospy.Rate(rateBumperReleased)
      
    return []
    
#----------------------------------------------------------------------------------

  def callbackScan(self,data):   

    ranges = np.array(data.ranges, dtype = "float32")
    
    close_distance_indices = ranges < self._distance    
    detection = (ranges[close_distance_indices].shape[0] > self._threshold)
    rospy.loginfo(detection)

    # publish the topic
    self._publisherLaser.publish(detection)


#----------------------------------------------------------------------------------

  def callbackBumper(self,data):
    if (data.state == BumperEvent.PRESSED):
      self._bumperPressed = True
    else:
      self._bumperPressed = False     

#----------------------------------------------------------------------------------

  def publishBumper(self):
    self._publisherBumper.publish(self._bumperPressed)
  
#----------------------------------------------------------------------------------
  
  def sleep(self):
    if (self._bumperPressed):
      self._rateBumperPressed.sleep()
    else:
      self._rateBumperReleased.sleep()
    
#----------------------------------------------------------------------------------

  def cleanup(self):
    pass
   
#----------------------------------------------------------------------------------
  
if __name__ == '__main__':
  # ros initialization and subscription
  rospy.init_node('collision_detector', anonymous=False)
  detector = CollisionDetector(rospy.Publisher("turtle_bale/collision_detection_laser", Bool, queue_size=10),rospy.Publisher("turtle_bale/collision_detection_bumper", Bool, queue_size=10))
  # Subscribe
  rospy.Subscriber("scan", LaserScan, detector.callbackScan)
  rospy.Subscriber("mobile_base/events/bumper", BumperEvent, detector.callbackBumper)
  # initialize the reset_parameters service
  rospy.Service('turtle_bale/detector/reset_parameters', Empty, detector.resetParameters)
  while not rospy.is_shutdown():
    try:
      detector.publishBumper()
      detector.sleep()
    except rospy.ROSInterruptException:
      rospy.loginfo(e)
  detector.cleanup()



